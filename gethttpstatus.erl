-module(gethttpstatus).

-export([start/1,httpstatus/1,getwebpage/1,printstatus/1]).
% -export([start/0]).

% this is a module which catches the http status of a webpage

start([Urlparameter]) ->
    getwebpage(Urlparameter).

getwebpage(Url) ->
    inets:start(),
    Result = httpstatus(Url),
    printstatus(Result).

httpstatus(Url) ->
    case  httpc:request(get, {Url, []}, [], []) of
        {_, {{_,Status,_},_,_}} -> Status;
        {error,_} -> failed_connect
    end.
    
printstatus(200) ->
    io:format("ok 200 ~n");
printstatus(failed_connect) ->
    io:format("nok ~w ~n", [failed_connect]);
printstatus(Code) ->
    io:format("nok ~w ~n", [Code]).

